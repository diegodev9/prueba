require 'rails_helper'

RSpec.describe Provider, type: :model do
  context 'when creating a provider' do
    before(:each) do
      @bank = Bank.create(name: 'bank1')
      @provider = Provider.create(name: 'test', nit:'901362343-4', contact: 'test_name', phone: '1234567890', bank_id: @bank.id, b_account: 22334455667799)
    end

    it 'phone can not be > 10 chars' do
      @provider.phone = '12345678910'

      expect(@provider).to_not be_valid
    end

    it 'bank account can not be > 15 char' do
      @provider.b_account = 2233445566778899

      expect(@provider).not_to be_valid
    end

    it 'nit number needs to have the form xxxxxxxxx-x' do
      @provider.nit = '1234567890'
      expect(@provider.nit).to_not match(/\d\d\d\d\d\d\d\d\d-\d/)

      @provider.nit = '123456789-0'
      expect(@provider.nit).to match(/\d\d\d\d\d\d\d\d\d-\d/)
    end

  end
end
