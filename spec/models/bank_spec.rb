require 'rails_helper'

RSpec.describe Bank, type: :model do
  context 'when creating a bank' do
    before(:each) do
      @bank = Bank.create(name: 'test name')
    end

    it 'can not be blank' do
      @bank.name = ''
      expect(@bank).to_not be_valid
    end

    it 'name should be < 50 chars' do
      @bank.name = 'j' * 51
      expect(@bank).to_not be_valid
    end

  end
end
