class DashboardController < ApplicationController
  def index
    @aria_index = ' bg-gray-900 text-white'
    @breadcrumb_top = "Dashboard"
    @banks = Bank.count
    @providers = Provider.count
  end
end
