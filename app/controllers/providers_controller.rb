class ProvidersController < ApplicationController
  before_action :set_provider, only: [:show, :edit, :update, :destroy]
  before_action :set_breadcrumb
  before_action :set_banks, only: [:new, :edit, :show, :create, :update]

  def index
    @providers = Provider.all.page(params[:page]).per(10)
    @aria_providers = ' bg-gray-900 text-white'
  end

  def new
    @provider = Provider.new
  end

  def edit
  end

  def show
    @bank_name = Bank.find_by id: @provider.bank_id
  end

  def create
    @provider = Provider.new(provider_params)

    respond_to do |format|
      if @provider.save
        format.html {redirect_to providers_path, notice: 'Proveedor creado'}
      else
        format.html { render :new }
        format.json { render json: @provider.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @provider.update(provider_params)
        format.html {redirect_to providers_path, notice: 'Proveedor modificado'}
      else
        format.html { render :edit }
        format.json { render json: @provider.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @provider.destroy
    respond_to do |format|
      format.html { redirect_to providers_path, notice: 'Proveedor eliminado' }
    end
  end

  private

  def set_provider
    @provider = Provider.find(params[:id])
  end

  def set_banks
    @banks_select = Bank.all.map{|bank| [bank.name, bank.id] }
  end

  def set_breadcrumb
    @breadcrumb_top = "Proveedores"
  end

  def provider_params
    params.require(:provider).permit(:name, :nit, :contact, :phone, :b_account, :bank_id)
  end
end
