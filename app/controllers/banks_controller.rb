class BanksController < ApplicationController
  before_action :set_bank, only: [:show, :edit, :update, :destroy]
  before_action :set_breadcrumb

  def index
    @banks = Bank.all.page(params[:page]).per(10)
    @aria_banks = ' bg-gray-900 text-white'
  end

  def new
    @bank = Bank.new
  end

  def edit
  end

  def show
  end

  def create
    @bank = Bank.new(bank_params)

    respond_to do |format|
      if @bank.save
        format.html {redirect_to banks_path, notice: 'Banco creado'}
      else
        format.json {render json: @bank.errors.full_messages, status: :unprocessable_entity}
        format.js {render new}
      end
    end
  end

  def update
    respond_to do |format|
      if @bank.update(bank_params)
        format.html {redirect_to banks_path, notice: 'Banco modificado'}
      else
        format.json { render json: @bank.errors.full_messages, status: :unprocessable_entity }
        format.js { render edit }
      end
    end
  end

  def destroy
    @bank.destroy
    respond_to do |format|
      format.html {redirect_to banks_path, notice: 'Banco eliminado'}
    end
  end

  private

  def set_bank
    @bank = Bank.find(params[:id])
  end

  def set_breadcrumb
    @breadcrumb_top = "Bancos"
  end

  def bank_params
    params.require(:bank).permit(:name)
  end
end
