class Provider < ApplicationRecord
  has_one :bank

  validates :phone, length: { maximum: 10 }
  validates :b_account, length: { maximum: 15 }
  validates :nit, format: { with: /\d\d\d\d\d\d\d\d\d-\d/, message: "debe tener la forma xxxxxxxxx-x" }
end
