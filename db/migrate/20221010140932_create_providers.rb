class CreateProviders < ActiveRecord::Migration[6.1]
  def change
    create_table :providers do |t|
      t.string :name
      t.integer :nit
      t.string :contact
      t.string :phone
      t.string :b_account
      t.references :bank, null: false, foreign_key: true

      t.timestamps
    end
  end
end
