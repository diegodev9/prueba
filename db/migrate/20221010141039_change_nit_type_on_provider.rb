class ChangeNitTypeOnProvider < ActiveRecord::Migration[6.1]
  def change
    change_column :providers, :nit, :string
  end
end
