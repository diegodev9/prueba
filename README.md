# README

dev env:
* git clone project
* yarn add tailwind
* yarn add jquery
* yarn add @popperjs/core
* yarn add @fortawesome/fontawesome-free
* bundle install
* ./bin/dev
* open browser on 127.0.0.1:3000

deploy prod:

* se debe contar con un servidor que tenga instalado: debian, rvm, npm, yarn, postgresql
* seguir la guia de phussionpassenger para montar el proyecto en produccion: https://www.phusionpassenger.com/library/walkthroughs/deploy/ruby/